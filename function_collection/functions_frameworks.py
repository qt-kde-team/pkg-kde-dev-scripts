#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024, Aurélien COUDERC <coucouf@debian.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import io
import requests
import subprocess
import urllib
import yaml

from functions import *

def migrateToKf6(pkg):
    updateVcsToSalsa(pkg, "Update Vcs links for the kf6 migration.")
    prefixSrcBinWithKf6(pkg)

def prefixSrcBinWithKf6(pkg:Package):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1

    msg = f"Add kf6- prefix to source and binary packages."

    control = pkg.path/"debian/control"

    control_changed = False

    with tempfile.NamedTemporaryFile() as tmpfile:
        for block_idx, block in enumerate(pkg.controlParagraphs()):
            if src_name := block.get('Source'):
                if src_name.startswith('libkf5'):
                    src_new_name = 'libkf6' + src_name.removeprefix('libkf5')
                if not '5' in src_name:
                    src_new_name = 'kf6-' + src_name
                if src_new_name != src_name:
                    # Source package was renamed
                    block['Source'] = src_new_name
                    control_changed = True
            elif pkg_name := block.get('Package'):
                if pkg_name.startswith('libkf5'):
                    pkg_new_name = 'libkf6' + pkg_name.removeprefix('libkf5')
                if not '5' in pkg_name:
                    pkg_new_name = 'kf6-' + pkg_name
                if pkg_new_name != pkg_name:
                    # Binary package was renamed
                    block['Package'] = pkg_new_name
                    control_changed = True
                    impactBinaryPkgRename(pkg, pkg_name, pkg_new_name)
            if block_idx > 0:
                tmpfile.write(b'\n')
            block.dump(tmpfile)

        tmpfile.flush()
        shutil.copyfile(tmpfile.name, control)

    if control_changed:
        wrap_and_sort( pkg, "debian/control")
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                        "debian/control",
                        ])
        pkg.git.index.commit(msg)

def impactBinaryPkgRename(pkg:Package, pkg_name:str, pkg_new_name:str):
    """Handles various impacts of renaming a binary package in the control file"""
    # Move {pkg_name}.lintian-overrides file
    l_o_path = f"debian/{pkg_name}.lintian-overrides"
    l_o_new_path = f"debian/{pkg_new_name}.lintian-overrides"
    if (pkg.path/l_o_path).exists():
        pkg.git.index.move([l_o_path, l_o_new_path])
        # Replace package name in lintian-overrides file
        subprocess.run(["sed", "-i", f"s/^{pkg_name}:/{pkg_new_name}:/", str(pkg.path/l_o_new_path)])
        pkg.git.index.add([l_o_new_path])

if __name__ == "__main__":
    pkg = getPackage()
