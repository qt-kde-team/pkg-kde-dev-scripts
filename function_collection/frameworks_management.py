import logging
import pydot
import pathlib

from functions import *
from functions_frameworks import *
from functions_plasma import *

import config
import salsa
from simple_pkg import simple_package

logging.getLogger().setLevel(logging.INFO)

KDEDIR = BASEDIR/"frameworks"

#Read tier data
tiers=[]
dotpath = sorted(config.tierdata.glob('frameworks.*.tier.dot'),reverse=True)[0]
version = ".".join(dotpath.name.split(".")[1:3])
logging.info(f"Using Frameworks {version} version data from file {dotpath}")
for subgraph in pydot.graph_from_dot_file(dotpath)[0].get_subgraph_list():
    tier=set()
    for node in subgraph.get_nodes():
        pkg_name = node.get_name()[1:-1]
        pkg_path = KDEDIR/pkg_name
        pkg_path_git = KDEDIR/f"{pkg_name}.git"
        if pkg_path.exists() and pkg_path_git.exists():
            raise Exception(f"Both {pkg_path} and {pkg_path_git} exist, don't know which to use")
        if not pkg_path.exists() and pkg_path_git.exists():
            pkg_path = pkg_path_git
        control = pkg_path/"debian/control"
        tier.add(getPackage(control))
    tiers.append(tier)

binaryPackages=set()
for p in itertools.chain(*tiers):
    if p is None: continue
    logging.info(f"Preparing package {p}")
    binaryPackages |= set(i.get("Package") for i in p.controlParagraphs() if i.get("Package"))
