#!/usr/bin/env python3
# -*- coding: utf8 -*-
# cmake_update_deps.py, parse cmake files, and add the corresponding debian
# dependency for the find_packages
#  Copyright © 2014-2016 Maximiliano Curia <maxy@gnuservers.com.ar>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import apt.cache
import argparse
import collections
import debian.deb822 as deb822
import debian.debian_support as debian_support
import itertools
import functools
import logging
import os
import pathlib
import ply.lex as lex
import ply.yacc as yacc
import re
import shutil
import subprocess
import tempfile
import warnings
import yaml

warnings.filterwarnings("ignore", category=UserWarning, message="cannot parse package relationship")

re_keyword = re.compile('^[A-Z_]+$')

CURDIR = pathlib.Path(__file__).absolute().parent
with (CURDIR/'cmake_deps.yml').open() as f:
    CMAKE_DEPS = yaml.safe_load(f)

class Dependency(
    collections.namedtuple(
        'Dependency', ['name', 'required', 'optional'])):

    def __new__(cls, name, required=None, optional=None):
        return super().__new__(cls, name, required, optional)


Version = debian_support.Version


class Parser(object):

    """
    Base class for a lexer/parser that has the rules defined as methods
    """
    tokens = ()
    precedence = ()

    def __init__(self, **kw):
        self.debug = kw.get('debug', 0)
        self.names = {}
        try:
            modname = os.path.split(os.path.splitext(__file__)[0])[1] + '_' + self.__class__.__name__
        except Exception:
            modname = 'parser' + '_' + self.__class__.__name__
        self.debugfile = modname + '.dbg'
        self.tabmodule = modname + '_' + 'parsetab'
        # print self.debugfile, self.tabmodule

        # Build the lexer and parser
        self.lexer = lex.lex(module=self, debug=self.debug)
        self.parser = yacc.yacc(module=self,
                                debug=self.debug,
                                debugfile=self.debugfile,
                                tabmodule=self.tabmodule)

    def run(self, fileobj):
        self.lexer.lineno = 1
        return self.parser.parse(fileobj.read(), tracking=True)


class CMakeParser(Parser):
    tokens = (
        'BRACKETARG',
        'COMMENT',
        'ID',
        'LBRACE',
        'RBRACE',
        'ARG',
        'WHITE',
        'DQUOTE',
        'VAR',
        'RCURLY',
        'VLT',
        'GT',
        'newline',
    )
    states = (
        ('args', 'exclusive'),
        ('bracketarg', 'exclusive'),
        ('linecomment', 'exclusive'),
        ('string', 'exclusive'),
        ('var', 'exclusive')
    )

    t_ignore = ' \t'
    t_args_ignore = ''
    t_bracketarg_ignore = ''
    t_linecomment_ignore = ''
    t_string_ignore = ''
    t_var_ignore = ''

    t_ID = r'[a-zA-Z][a-zA-Z0-9_]*'

    def t_COMMENT_BRACKET(self, t):
        r'\#\[=*\['
        self.bracketarg_len = len(t.value) - 1
        self.lexer.push_state('bracketarg')
        t.type = 'COMMENT'
        return t

    def t_COMMENT(self, t):
        r'\#'
        self.lexer.push_state('linecomment')
        return t

    def t_LBRACE(self, t):
        r'\('
        self.lexer.push_state('args')
        return t

    def t_args_COMMENT_BRACKET(self, t):
        r'\#\[=*\['
        self.bracketarg_len = len(t.value) - 1
        self.lexer.push_state('bracketarg')
        t.type = 'COMMENT'
        return t

    def t_args_COMMENT(self, t):
        r'\#'
        self.lexer.push_state('linecomment')
        return t

    def t_args_LBRACE(self, t):
        r'\('
        self.lexer.push_state('args')
        return t

    def t_args_RBRACE(self, t):
        r'\)'
        self.lexer.pop_state()
        return t

    t_args_ARG = r'[^\s()\#"\\;$]+'

    def t_args_ESC_ID(self, t):
        r'\\[()\#" \\$@^;]'
        t.value = t.value[1]
        t.type = 'ARG'
        return t

    def t_args_ESC_ENC(self, t):
        r'\\[ntr]'
        if t.value == '\\n':
            t.value = '\n'
        elif t.value == '\\t':
            t.value = '\t'
        elif t.value == '\\r':
            t.value = '\r'
        t.type = 'ARG'
        return t

    def t_args_ENVVAR(self, t):
        r'\$ENV{'
        self.lexer.push_state('var')
        t.type = 'VAR'
        return t

    def t_args_VAR(self, t):
        r'\${'
        self.lexer.push_state('var')
        return t

    def t_args_VLT(self, t):
        r'\$<'
        self.lexer.push_state('var')
        return t

    def t_args_WHITE(self, t):
        r'[\s;]+'
        self.lexer.lineno += t.value.count('\n')
        return t

    def t_args_DQUOTE(self, t):
        r'"'
        self.lexer.push_state('string')
        return t

    def t_args_BRACKETARG(self, t):
        r'\[=*\['
        self.bracketarg_len = len(t.value)
        self.lexer.push_state('bracketarg')
        return t

    def t_string_DQUOTE(self, t):
        r'"'
        self.lexer.pop_state()
        return t

    def t_string_ENVVAR(self, t):
        r'\$ENV{'
        self.lexer.push_state('var')
        t.type = 'VAR'
        return t

    def t_string_VAR(self, t):
        r'\${'
        self.lexer.push_state('var')
        return t

    def t_string_ARG(self, t):
        r'[^\\"]+'
        return t

    def t_string_ESC_ID(self, t):
        r'\\[()\#" \\$@^;]'
        t.value = t.value[1]
        t.type = 'ARG'
        return t

    def t_string_ESC_ENC(self, t):
        r'\\[ntr]'
        if t.value == '\\n':
            t.value = '\n'
        elif t.value == '\\t':
            t.value = '\t'
        elif t.value == '\\r':
            t.value = '\r'
        t.type = 'ARG'
        return t

    def t_string_ESC_CONT(self, t):
        r'\\\n'
        self.lexer.lineno += 1

    t_var_ARG = r'[^\s()\#"\\;$}>]+'

    def t_var_ENVVAR(self, t):
        r'\$ENV{'
        self.lexer.push_state('var')
        t.type = 'VAR'
        return t

    def t_var_VAR(self, t):
        r'\${'
        self.lexer.push_state('var')
        return t

    def t_var_VLT(self, t):
        r'\$<'
        self.lexer.push_state('var')
        return t

    def t_var_RCURLY(self, t):
        r'}'
        self.lexer.pop_state()
        return t

    def t_var_GT(self, t):
        r'>'
        self.lexer.pop_state()
        return t

    def t_bracketarg_BRACKETARG(self, t):
        r'\]=*\]'
        if len(t.value) == self.bracketarg_len:
            self.lexer.pop_state()
            return t
        t.type = 'ARG'
        return t

    def t_bracketarg_newline(self, t):
        r'\n+'
        self.lexer.lineno += len(t.value)
        t.type = 'ARG'
        return t

    def t_bracketarg_ARG(self, t):
        r'.'
        return t

    t_linecomment_ARG = r'.+'

    def t_linecomment_newline(self, t):
        r'\n+'
        self.lexer.lineno += len(t.value)
        self.lexer.pop_state()
        return t

    def t_newline(self, t):
        r'\n+'
        self.lexer.lineno += len(t.value)

    def t_ANY_error(self, t):
        logging.error(
            "Illegal character '{}' at line {}".format(t.value,
                                                       self.lexer.lineno))
        self.lexer.skip(1)

    ##
    # Parser yacc rules
    ##

    def p_elements_optional(self, p):
        '''elements_optional : elements
                             | empty'''
        p[0] = p[1] if p[1] else []

    def p_elements(self, p):
        '''elements : elements element
                    | element'''
        aux = p[len(p) - 1]
        if aux and len(p) == 3 and p[1]:
            p[0] = p[1] + [aux]
            # logging.debug('element {}'.format(aux))
        elif len(p) == 3 and p[1]:
            p[0] = p[1]
        elif aux:
            p[0] = [aux]
            # logging.debug('element {}'.format(aux))

    def p_element(self, p):
        '''element : command
                   | comment'''
        if p[1]:
            p[0] = p[1]

    def p_command(self, p):
        '''command : ID LBRACE arguments_optional RBRACE'''
        command = p[1]
        args = p[3]

        p[0] = [command] + args

    def p_arguments_optional(self, p):
        '''arguments_optional : arguments argument_separators
                              | arguments
                              | argument_separators
                              | empty'''
        if not p[1]:
            p[0] = []
        else:
            p[0] = p[1]

    def p_arguments(self, p):
        '''arguments : arguments argument_separators argument
                     | argument_separators argument
                     | argument
        '''
        aux = [p[len(p) - 1]]
        if len(p) == 4:
            p[0] = p[1] + aux
        else:
            p[0] = aux

    def p_argument_separators(self, p):
        '''argument_separators : argument_separators argument_separator
                               | argument_separator
        '''
        p[0] = ''

    def p_arguments_separator(self, p):
        '''argument_separator : comment
                              | WHITE
        '''
        p[0] = ''

    def p_argument(self, p):
        '''argument : argument_parts_optional LBRACE arguments_optional RBRACE
                    | argument_parts
        '''
        if len(p) == 5:
            p[0] = [p[1], p[2]]
        else:
            p[0] = p[1][0] if len(p[1]) == 1 else p[1]

    def p_argument_parts_optional(self, p):
        '''argument_parts_optional : argument_parts
                                   | empty'''

        if p[1]:
            p[0] = p[1]
        else:
            p[0] = []

    def p_argument_parts(self, p):
        '''argument_parts : argument_parts argument_part
                          | argument_part'''
        aux = [p[len(p) - 1]]
        if len(p) == 3:
            p[0] = p[1] + aux
        else:
            p[0] = aux

    def p_argument_part(self, p):
        '''argument_part : var
                         | bracket_argument
                         | quoted_argument
                         | unquoted_argument'''
        p[0] = p[1]

    def p_bracket_argument(self, p):
        '''bracket_argument : BRACKETARG args_optional BRACKETARG'''
        p[0] = p[2]

    def p_quoted_argument(self, p):
        '''quoted_argument : DQUOTE quoted_argument_parts_optional DQUOTE'''
        p[0] = p[2]

    def p_quoted_argument_parts_optional(self, p):
        '''quoted_argument_parts_optional : quoted_argument_parts
                                          | empty'''
        p[0] = p[1] if p[1] else ''

    def p_quoted_argument_parts(self, p):
        '''quoted_argument_parts : quoted_argument_parts quoted_argument_part
                                 | quoted_argument_part'''
        aux = [p[len(p) - 1]]
        if len(p) == 3:
            p[0] = p[1] + aux
        else:
            p[0] = aux

    def p_quoted_argument_part(self, p):
        '''quoted_argument_part : args
                                | var'''
        p[0] = p[1]

    def p_unquoted_argument(self, p):
        '''unquoted_argument : args'''
        p[0] = p[1]

    def p_var(self, p):
        '''var : VAR var_parts RCURLY
               | VLT var_parts GT'''
        # logging.debug(p[:])
        if p[1] == '$ENV{':
            p[0] = ('ENV', p[2])
        else:
            p[0] = ('VAR', p[2])

    def p_var_parts(self, p):
        '''var_parts : var_parts var_part
                     | var_part'''
        aux = p[len(p) - 1]
        if len(p) == 3:
            p[0] = [p[1]] + [aux]
        else:
            p[0] = aux

    def p_var_part(self, p):
        '''var_part : args
                    | var'''
        p[0] = p[1]

    def p_comment(self, p):
        '''comment : COMMENT args_optional comment_end'''
        p[0] = ''
        # p[0] = 'COMMENT: {}'.format(p[1])

    def p_args_optional(self, p):
        '''args_optional : args
                         | empty'''
        p[0] = p[1] if p[1] else ''

    def p_comment_end(self, p):
        '''comment_end : newline
                       | BRACKETARG'''

    def p_args(self, p):
        '''args : args arg
                | arg'''
        p[0] = ''.join(p[1:])

    def p_arg(self, p):
        '''arg : ARG'''
        p[0] = p[1]

    def p_empty(self, p):
        'empty :'
        pass

    def p_error(self, p):
        if p:
            logging.error("Syntax error at '%s' at %s" % (p.value, p.lexpos))
        else:
            logging.error('Syntax error at EOF')

    def mylex(self, lines):
        self.lexer.lineno = 1
        tokens = []
        for line in lines:
            tokens.append('{}:'.format(self.lexer.lineno))
            self.lexer.input(line)
            token = self.lexer.token()
            while token:
                tokens.append(token)
                token = self.lexer.token()
        return tokens

    def product_mixed(self, spec):
        # logging.debug(spec)
        xs = list(map(lambda a: ''.join(a), itertools.product(*spec)))
        if len(xs) == 1:
            return xs[0]
        return xs

    def expand(self, values):
        xs = []
        if isinstance(values, tuple) and values[0] in ('VAR', 'ENV'):
            values = [values]

        for value in values:
            if isinstance(value, list):
                value = self.expand(value)
            if value[0] == 'VAR':
                var_name = self.expand(value[1])
                if var_name in self.variables:
                    logging.debug('Append: {} + {}'.format(xs,
                                                           self.variables[var_name]))
                    xs.append(self.variables[var_name])
                else:
                    logging.error('var {} not found'.format(var_name))
                    xs.append('')
            elif value[0] == 'ENV':
                var_name = self.expand(value[1])
                if var_name in os.environ:
                    xs.append(os.environ[var_name])
                else:
                    logging.error('env {} not found'.format(var_name))
                    xs.append('')
            else:
                xs.append(value)
            if not isinstance(xs[-1], list):
                xs[-1] = [xs[-1]]
        return self.product_mixed(xs)

    def relative_dirname_join(self, *args):
        dirname = os.path.dirname(self.file_stack[-1])
        return os.path.join(dirname, *args)

    def requirement_merge(self, dependency):
        if dependency.name not in self.requirements:
            self.requirements[dependency.name] = dependency
            return
        current = self.requirements[dependency.name]
        if dependency.required and current.required:
            if dependency.required > current.required:
                current = current._replace(required=dependency.required)
        elif dependency.required:
            current = current._replace(required=dependency.required)
        if dependency.optional and current.optional:
            if dependency.optional > current.optional:
                current = current._replace(optional=dependency.optional)
        elif dependency.optional:
            current = current._replace(optional=dependency.optional)

        self.requirements[dependency.name] = current

    def cmake_minimum_required(self, invocation):
        logging.debug(invocation)
        version = None

        args = invocation[1:]
        assert(args[0].lower() == 'version')

        version = self.expand(args[1])
        d = Dependency('cmake', required=Version(version))
        # logging.debug(d)

        self.requirement_merge(d)

    def add_subdirectory(self, invocation):
        # logging.debug(invocation)
        subdir = self.expand(invocation[1])
        filename = self.relative_dirname_join(subdir, 'CMakeLists.txt')
        if not os.path.exists(filename):
            logging.warning(
                'File: {} not found, cmake project incomplete?'.format(
                    filename))
            return
        # logging.debug(filename)
        self.run_file(filename)

    def cmake_set(self, invocation):
        # logging.debug(invocation)
        var_name = self.expand(invocation[1])
        if isinstance(var_name, list):
            invocation[2:2] = var_name[1:]
            var_name = var_name[0]
        value = []
        for arg in invocation[2:]:
            if arg in ('CACHE', 'PARENT_SCOPE'):
                break
            expanded_arg = self.expand(arg)
            if isinstance(expanded_arg, list):
                value.extend(expanded_arg)
            else:
                value.append(self.expand(arg))
        if len(value) == 1:
            value = value[0]

        logging.debug('{} = {}'.format(var_name, value))
        if not isinstance(var_name, list):
            self.variables[var_name] = value

    def include(self, invocation):
        filename = self.relative_dirname_join(self.expand(invocation[1]))
        if not os.path.exists(filename):
            if invocation[1] not in ('ECMAddTests', 'ECMGeneratePriFile', 'ECMSetupVersion', 'KDEInstallDirs', 'KDECMakeSettings', 'KDEFrameworkCompilerSettings', 'ECMGenerateHeaders', 'ECMQtDeclareLoggingCategory', 'ECMAddQch', 'ECMMarkNonGuiExecutable', 'FeatureSummary', 'GenerateExportHeader', 'ConfigureChecks', 'ECMMarkAsTest', 'CheckIncludeFile', 'ECMAddQtDesignerPlugin', 'ECMSetupQtPluginMacroNames', 'CheckFunctionExists', 'CheckSymbolExists', 'ECMInstallIcons', 'ECMAddAppIcon'):
                logging.warning(
                    'File: {} not found, TODO: search sys paths'.format(
                        filename))
            return
        self.run_file(filename)

    def find_package(self, invocation):
        # find_package(pkgname [version] [exact] [quiet] [module] [required]
        # [[components] names] [optional_components names])
        logging.debug(invocation)

        re_version = re.compile('^[0-9.]+$')

        package = self.expand(invocation[1])
        version = None
        components = []
        optional_components = []
        required = False
        i = 2
        keywords = {
            'EXACT': 0, 'QUIET': 0, 'CONFIG': 0, 'MODULE': 0,
            'NO_MODULE': 0, 'NO_POLICY_SCOPE': 0, 'REQUIRED': 0,
            'NO_DEFAULT_PATH': 0, 'NO_CMAKE_ENVIRONMENT_PATH': 0,
            'NO_CMAKE_PATH': 0, 'NO_SYSTEM_ENVIRONMENT_PATH': 0,
            'NO_CMAKE_PACKAGE_REGISTRY': 0, 'NO_CMAKE_BUILDS_PATH': 0,
            'NO_CMAKE_SYSTEM_PATH': 0,
            'NO_CMAKE_SYSTEM_PACKAGE_REGISTRY': 0,
            'CMAKE_FIND_ROOT_PATH_BOTH': 0,
            'CMAKE_FIND_ROOT_PATH_BOTH': 0,
            'ONLY_CMAKE_FIND_ROOT_PATH': 0,
            'NO_CMAKE_FIND_ROOT_PATH': 0,
            'COMPONENTS': 1,
            'OPTIONAL_COMPONENTS': 2,
            'NAMES': 4, 'CONFIGS': 4, 'HINTS': 4, 'PATHS': 4,
            'PATH_SUFFIXES': 4,
        }
        state = 0
        for i, part in enumerate(invocation[2:],2):
            arg = self.expand(part)
            # Handle components lists used in find_package like kguiaddons does with:
            #   set(_qtgui_find_components COMPONENTS Private)
            #   find_package(Qt${QT_MAJOR_VERSION}Gui ${REQUIRED_QT_VERSION} REQUIRED NO_MODULE ${_qtgui_find_components})
            if type(arg) == list:
                for unit_arg in arg:
                    components.append(unit_arg)
            elif arg not in keywords:
                if state == 0 and re_version.match(arg):
                    logging.debug('Is a version: {} ?'.format(arg))
                    version = Version(arg)
                elif state == 0 and part == ('VAR', 'KF5_MIN_VERSION'):
                    version = "KF5_MIN_VERSION"
                elif state == 0 or state & 1:
                    components.append(arg)
                elif state & 2:
                    optional_components.append(arg)
            else:
                state = keywords[arg]
                if arg == 'REQUIRED':
                    required = True

        kw = {}

        if (required or optional_components):
            kw['required'] = version if version else Version('0')
        else:
            kw['optional'] = version

        if not (components or optional_components):
            self.requirement_merge(Dependency(package, **kw))
        else:
            for c in components:
                self.requirement_merge(Dependency(
                    '{}{}'.format(package, c),
                    **kw))
            for c in optional_components:
                self.requirement_merge(Dependency(
                    '{}{}'.format(package, c), optional=version))

    def __init__(self, **kw):
        super().__init__(**kw)
        self.files = {}
        self.file_stack = collections.deque()
        self.variables = {
            'BUILD_WITH_QT6': 'ON',
            'CMAKE_SOURCE_DIR': '.',
            'QT_MAJOR_VERSION': '6',
        }
        self.packages = {}
        self.functions = {}
        self.requirements = {}

        self.known_commands = {
            'add_subdirectory': self.add_subdirectory,
            'include': self.include,
            'cmake_minimum_required': self.cmake_minimum_required,
            'set': self.cmake_set,
            'find_package': self.find_package,
            'macro_optional_find_package': self.find_package,
        }

    def add_file(self, filename):
        if filename in self.files:
            return
        f = open(filename)
        self.lexer.lineno = 1
        self.files[filename] = self.parser.parse(f.read(), tracking=True)
        return self.files[filename]

    def call(self, function, args):
        pass

    def run_file(self, filename):
        logging.debug('About to process: {}'.format(filename))
        invocations = self.add_file(filename)
        if not invocations:
            return
        logging.debug('Start of: {}'.format(filename))
        self.file_stack.append(filename)
        for invocation in invocations:
            command = invocation[0].lower()
            if command in self.known_commands:
                self.known_commands[command](invocation)
            elif command in self.functions:
                self.call(self.functions[command], invocation[1:])
            else:
                # logging.debug(command)
                pass
        self.file_stack.pop()
        logging.debug('End of: {}'.format(filename))

        # queue = collections.deque(filenames)
        # elements_queue = collections.deque()
        # stack = collections.deque()
        # while queue:
        #     filename = queue.popleft()
        #     elements = self.add_file(filename)
        #     if not elements:
        #         continue

        #     for element in self.files[filename]

    def print_requirements(self):
        for name, dependency in self.requirements.items():
            print(dependency)


@functools.total_ordering
class DebPackageInfo(object):

    def __init__(self, name, uversion=None, epoch=None, mangler=None, suite=None, versions_mapping=None):
        self.name = name
        self._epoch = epoch
        if uversion is None:
            uversion = Version('0')
        self.uversion = uversion
        if not mangler:
            mangler = self._mangler
        self.mangler = mangler
        self.suite = suite
        self.versions_mapping = versions_mapping
        if self.versions_mapping and str(uversion) in self.versions_mapping:
            self.uversion = Version(versions_mapping[str(uversion)]['map_to'])
            logging.debug('Mapping %s version %s to %s', self.name, uversion, self.uversion)

    @property
    def epoch(self):
        return self._epoch if self._epoch is not None else 0

    def _mangler(self, upstream_version):
        return '{}{}~'.format(
            '{}:'.format(self.epoch) if self.epoch else '',
            upstream_version)

    def debian_version(self, uversion=None):
        if not uversion and self.uversion:
            uversion = str(self.uversion)
        if uversion == '0':
            return ''
        return self.mangler(uversion)

    def __str__(self):
        s = self.name
        version = self.debian_version()
        if not version or version == '0':
            return s
        s += ' (>= {})'.format(version)
        return s

    def __lt__(self, other):
        return ((self.name, self.epoch, self.uversion) <
                (other.name, other.epoch, self.uversion))

    def __eq__(self, other):
        return ((self.name, self.epoch, self.uversion) ==
                (other.name, other.epoch, self.uversion))


def handlePimSuite(versionString):
    intToExt = {
        '0': '24.02',
        '1': '24.05',
        '2': '24.08',
        '3': '24.12'
    }
    v = str(versionString).split('.')

    while len(v) < 3:
        v.append('0')

    if v[0] == '6':
        return Version('{}.{}'.format(intToExt[v[1]], v[2]))

    return versionString


class ReqToDebianPkg(object):

    def __init__(self):
        self.not_found = {}
        self.needed = {}
        self.optional = {}

    name = 'name'
    epoch = 'epoch'
    suite = 'suite'
    known = CMAKE_DEPS

    suites = {
        'pim': handlePimSuite,
    }

    manglers = {
        'noVersion': lambda v: ''
    }

    def process(self, requirements):
        needed = {}
        optional = {}
        not_found = {}
        ecm_version = None
        apt_cache = apt.cache.Cache()
        for name, dependency in requirements.items():
            kwargs = self.known.get(name, not_found)
            if kwargs is not_found:
                not_found[name] = dependency
                continue
            elif kwargs is None:
                # Not in Debian
                continue
            if 'mangler' in kwargs:
                kwargs['mangler'] = self.manglers.get(kwargs['mangler'])

            logging.debug("Processing %s: %s", name, dependency)

            dname = kwargs['name']

            # Get epoch from APT installable candidate
            candidate_ver = apt_cache[dname].candidate.version
            if ":" in candidate_ver:
                kwargs['epoch'] = candidate_ver.split(":")[0]

            if dependency.required:
                req = dependency.required
                if 'suite' in kwargs:
                    req = self.suites.get(kwargs['suite'], lambda x: x)(req)
                if ((dname not in needed) or
                        (req > needed[dname].uversion)):
                    kwargs['uversion'] = req
                    needed[dname] = DebPackageInfo(**kwargs)

            if ((not dependency.required) or
                (dependency.optional and
                    dependency.optional >= dependency.required)):

                req = dependency.optional
                if req:
                    if 'suite' in kwargs:
                        req = self.suites.get(kwargs['suite'], lambda x: x)(req)
                    kwargs['uversion'] = req

                if ((dname not in optional) or
                        (req > optional[dname].uversion)):
                    optional[dname] = DebPackageInfo(**kwargs)

            if kwargs['name'] == 'extra-cmake-modules':
                ecm_version = kwargs['uversion']

        for name in needed:
            if name in optional and \
               needed[name].uversion >= optional[name].uversion:
                del optional[name]

        if ecm_version:
            for dep in itertools.chain(needed.values(), optional.values()):
                if dep.uversion == "KF5_MIN_VERSION":
                    dep.uversion = ecm_version

        self.not_found = not_found
        self.needed = needed
        self.optional = optional

    def __str__(self) -> str:
        s = ""
        if self.not_found:
            s += 'Not known packages:\n'
            for item in sorted(self.not_found.values()):
                s += "  {}\n".format(item)
        if self.needed:
            s += 'Needed build-deps:\n'
            for item in sorted(self.needed.values()):
                s += "  {}\n".format(item)
        if self.optional:
            s += 'Optional build-deps:\n'
            for item in sorted(self.optional.values()):
                s += "  {}\n".format(item)
        s = s[:-1]
        return s

def get_blacklist(package_dir):
    cmd = ['dh_listpackages']
    output = subprocess.check_output(cmd, cwd=package_dir).decode('utf8')

    return set(output.split())


# Updated version of deb822.PkgRelation.str that handles the 'archqual' field
def rels2str(rels):
    """Format to string structured inter-package relationships

    Perform the inverse operation of parse_relations, returning a string
    suitable to be written in a package stanza.
    """
    def pp_arch(arch_spec):
        return '%s%s' % (
            '' if arch_spec.enabled else '!',
            arch_spec.arch,
        )

    def pp_restrictions(restrictions):
        s = []
        for term in restrictions:
            s.append('%s%s' % (
                '' if term.enabled else '!',
                term.profile
            )
            )
        return '<%s>' % ' '.join(s)

    def pp_atomic_dep(dep):
        s = dep['name']
        if dep.get('archqual') is not None:
            s += ':%s' % dep['archqual']
        if dep.get('version') is not None:
            s += ' (%s %s)' % dep['version']
        if dep.get('arch') is not None:
            s += ' [%s]' % ' '.join(map(pp_arch, dep['arch']))
        if dep.get('restrictions') is not None:
            s += ' %s' % ' '.join(map(pp_restrictions, dep['restrictions']))
        return s

    def pp_or_dep(deps):
        return ' | '.join(map(pp_atomic_dep, deps))

    return ', '.join(map(pp_or_dep, rels))


def update_field(section, field, fun, reqs, blacklist):
    logging.debug("update_field section={section}, field={field}, blacklist={blacklist}")
    value = section.get(field)
    if not value:
        return 0
    rels = deb822.PkgRelation.parse_relations(value)
    changes = fun(rels, reqs, blacklist)
    if changes:
        section[field] = rels2str(rels)
    return changes


def add_required(rels, reqs, blacklist):
    found = set()
    changes = 0
    for rel in rels:
        for item in rel:
            found.add(item['name'])
    for req in reqs.needed:
        if req in found:
            continue
        if req in blacklist:
            continue
        item = {
            'name': req,
            'version': None,
            'arch': None,
            'archqual': None,
            'restrictions': None,
        }
        rels.append([item])
        changes += 1
    return changes


def check_req_version(item, req_dict):
    name = item['name']
    if '$' in name:
        # subst var, leave alone
        return 0
    if name not in req_dict:
        return 0
    req_version = req_dict[name].debian_version()
    if not req_version:
        return 0

    if not item['version']:
        item['version'] = ('>=', req_version)
        return 1

    cur_version = item['version'][1]
    if cur_version.startswith('$'):
        # subst var, leave alone
        return 0

    if debian_support.version_compare(cur_version, req_version) < 0:
        item['version'] = ('>=', req_version)
        return 1
    return 0


def bump_versions(rels, reqs, blacklist):

    changes = 0
    for rel in rels:
        for item in rel:
            for req_dict in reqs.needed, reqs.optional:
                changes += check_req_version(item, req_dict)
    return changes


def update_control(filename, reqs, blacklist):
    control_file = open(filename)
    changes = 0

    logging.info(f'Updating control file from reqs:\n{reqs}')

    with tempfile.NamedTemporaryFile() as tmpfile:
        for i, section in enumerate(
                deb822.Deb822.iter_paragraphs(control_file)):
            if i:
                tmpfile.write(b'\n')
            if i == 0:
                changes += update_field(section, 'Build-Depends',
                                        add_required, reqs, blacklist)

            for field in ('Build-Depends', 'Build-Depends-Indep', 'Depends',
                          'Recommends', 'Suggests'):
                changes += update_field(section, field, bump_versions, reqs,
                                        blacklist)

            section.dump(tmpfile)
        if changes:
            tmpfile.flush()
            shutil.copyfile(tmpfile.name, filename)


def process_options():

    kw = {
        'format': '[%(levelname)s] %(message)s',
    }

    arg_parser = argparse.ArgumentParser(
        description='Update debian control files from cmake information.')
    arg_parser.add_argument('--no-act', action='store_true')
    arg_parser.add_argument('-c', '--control', default='debian/control')
    arg_parser.add_argument('-m', '--commit-message', default='Update build-deps and deps with the info from cmake')
    arg_parser.add_argument('-l', '--changelog', default='debian/changelog')
    arg_parser.add_argument('--add-changelog-item', action='store_true')
    arg_parser.add_argument('-d', '--package-dir', default='repo')
    arg_parser.add_argument('-u', '--upstream-dir', default='upstream')
    arg_parser.add_argument('--debug', action='store_true')
    arg_parser.add_argument('--wrap-and-sort-opt', '-w', action='append',
                            default=[])
    arg_parser.add_argument('cmake_files', nargs='*',
                            default=['CMakeLists.txt'])
    args = arg_parser.parse_args()

    if args.debug:
        kw['level'] = logging.DEBUG
    if not 'level' in kw:
        kw['level'] = logging.INFO

    logging.basicConfig(**kw)

    return args


def commit(options):
    def staged_changes():
        git_exit_code=0
        try:
            status = subprocess.check_output(
                ['git', 'diff', '--cached', '--exit-code', '--quiet'], cwd=options.package_dir,
                universal_newlines=True)
        except subprocess.CalledProcessError as grepexc:
            git_exit_code = grepexc.returncode
        return git_exit_code

    def wrap_and_sort(filename=None):
        cmd = ['wrap-and-sort', '-t']
        cmd += options.wrap_and_sort_opt
        if filename:
            cmd.extend(['-f', filename])
        subprocess.call(cmd, cwd=options.package_dir)

    wrap_and_sort(options.control)
    subprocess.call(['git', 'add', options.control], cwd=options.package_dir)
    if not staged_changes():
        return

    if options.add_changelog_item:
        subprocess.call(['dch', '-a', options.commit_message], cwd=options.package_dir)
        subprocess.call(['git', 'add', options.changelog], cwd=options.package_dir)

    subprocess.call(['git', 'add', options.control], cwd=options.package_dir)
    subprocess.call(['git', 'commit', '-m', options.commit_message],
                    cwd=options.package_dir)


def main():
    options = process_options()
    parser = CMakeParser(debug=options.debug)

    for cmake_file in options.cmake_files:
        full_path = os.path.join(options.upstream_dir, cmake_file)
        if not os.path.exists(full_path):
            logging.warning(
                'File: {} not found, not cmake based project?'.format(
                    full_path))
            continue
        parser.run_file(full_path)

    debianizer = ReqToDebianPkg()
    debianizer.process(parser.requirements)

    blacklist = get_blacklist(options.package_dir)

    if not options.no_act:
        control_file = os.path.join(options.package_dir, options.control)
        update_control(control_file, debianizer, blacklist)

        commit(options)


if __name__ == '__main__':
    main()
