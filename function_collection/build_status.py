#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2019 Sandro Knauß <hefee@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from collections import defaultdict
from debian.debian_support import Version
from debian import deb822
import pathlib
import pydot
import re
import sys
import requests
import subprocess
from tqdm import tqdm
from multiprocessing import Pool

from .functions import BASEDIR, getPackage
from . import salsa



summary=b"""+------------------------------------------------------------------------------+
| Summary                                                                      |
+------------------------------------------------------------------------------+"""

def status_local(pkg):
    state = None
    name = pkg.dscPath.stem + "_source.build"
    sourcePath = pkg.dscPath.with_name(name)
    name = pkg.dscPath.stem + "_amd64.build"
    amd64Path = pkg.dscPath.with_name(name)
    if sourcePath.exists():
        state = "waiting"
        if amd64Path.exists():
            if sourcePath.stat().st_atime > amd64Path.stat().st_atime:
                return state

    try:
        text = amd64Path.read_bytes()
        m = re.search(b"^Status:\s*(.*)$",text[text.find(summary):], re.M)
        if m:
            return m.group(1).decode()
        return "started"
    except FileNotFoundError:
            return state

def status_salsa(pkg):
    salsaPackage = salsa.SalsaPackage(pkg)
    return salsaPackage.getBuildStatus()

getStatus = status_local
getStatus = status_salsa

STATUS={"waiting":"blue",
        "started":"yellow",
        "running":"yellow",
        "successful":"green",
        "success":"green",
        "attempted":"red",
        "failed":"red",
        "given-back":"red",
       }


if __name__ == "__main__":

    product = sys.argv[1]
    version = sys.argv[2]
    qtkdeteam = False
    if len(sys.argv) > 3:
        qtkdeteam = True


    fname = f"{product}.{version}.tier.dot"
    buildname = f"{product}.{version}.tier.status.dot"
    pngname = f"{product}.{version}.tier.status.png"
    curdir = pathlib.Path(__file__).parent

    graph = pydot.graph_from_dot_file(curdir/fname)[0]

    kdedir = BASEDIR/"kde"

    #Read tier data
    packages = set()
    for subgraph in graph.get_subgraph_list():
        for node in subgraph.get_nodes():
            pkg_name = node.get_name()[1:-1]
            pkg_path = kdedir/pkg_name
            control = pkg_path/"debian/control"
            packages.add(getPackage(control))

    def _getStatus(pkg):
        if pkg.changelog.version.upstream_version < Version(version):
            return (pkg, None)
        ret = getStatus(pkg)
        return (pkg, ret)

    with Pool() as pool:
        iterator = pool.imap(_getStatus, tqdm(packages), chunksize=1)

        statuses = defaultdict(set)
        for pkg, status in iterator:
            statuses[status].add(pkg)

    for status, pks in statuses.items():
        if status != "success":
            print(f"{status}: {','.join(i.name for i in pks)}")
        if not status:
            continue
        color = STATUS[status]
        for pkg in pks:
            n = pydot.Node(f'"{pkg.path.name}"',color=color, penwidth=3)
            graph.add_node(n)

    if qtkdeteam:
        onqtkdeteam = set()
        r = requests.get('https://qt-kde-team.debian.net/debian/dists/UNRELEASED/main/binary-amd64/Packages')
        for p in deb822.Packages.iter_paragraphs(r.text):
            try:
                pkg = next(filter(lambda i:i.name == p.get('Source'), packages))
            except StopIteration:
                continue
            if p.get('Source') in (i.name for i in packages):
                if p.get_version().upstream_version >= Version(version):
                    onqtkdeteam.add(pkg.path.name)

        for i in onqtkdeteam:
            n = pydot.Node(f'"{i}"',color='blue', penwidth=3)
            graph.add_node(n)

    graph.write(curdir/buildname)
    subprocess.check_call(["dot", "-T","png", "-o", pngname, buildname], cwd=curdir)
