import pathlib
import yaml

__all__ = ["BASEDIR", "CONFIG", "tierdata"]

curfile = pathlib.Path(__file__)

with curfile.with_name("config.yml").open() as f:
    CONFIG = yaml.safe_load(f)  # Configuration of function_collection

BASEDIR = pathlib.Path(CONFIG['basedir'])
tierdata = curfile.parent.with_name("tierdata")     # Where to find the tierdata
