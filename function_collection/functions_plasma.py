import io
import requests
import urllib
import yaml

from functions import *

def updateGitRemote(pkg):
    pkg.git.remotes.origin.set_url(f"qt-kde-team:{pkg.salsaGroup}/{pkg.salsaName}.git")


def projectsApi(pkg):
    data = requests.get('https://projects.kde.org/api/v1/identifier/' + pkg.upstreamName).json()
    return data


def updateHomepagetoInvent(pkg):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1

    msg = f"Update Homepage link to point to new invent.kde.org"

    api = projectsApi(pkg)
    url = f"https://invent.kde.org/{api['repo']}"
    control = pkg.path/"debian/control"

    changed = False

    with tempfile.NamedTemporaryFile() as tmpfile:
        for block in pkg.controlParagraphs():
            if block.get("Source"):
                if block['Homepage'] != url:
                    block['Homepage'] = url
                    changed = True
                block.dump(tmpfile)
                continue
            tmpfile.write(b'\n')
            block.dump(tmpfile)

        tmpfile.flush()
        shutil.copyfile(tmpfile.name, control)

    if changed:
        wrap_and_sort( pkg, "debian/control")
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                        "debian/control",
                        ])
        pkg.git.index.commit(msg)


def updateCopyrightSourcetoInvent(pkg):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1
    msg = f'Update field Source in debian/copyright to invent.kde.org move.'

    api = projectsApi(pkg)
    url = f"https://invent.kde.org/{api['repo']}"
    copyright_file = pkg.path/"debian/copyright"

    changed = False
    c = copyright.Copyright(copyright_file.open())
    if c.header.source != url:
        c.header.source = url
        copyright_file.write_text(c.dump())
        changed = True

    if changed:
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                           "debian/copyright"
                           ])
        pkg.git.index.commit(msg)


def updateUpstreamContact(pkg, contactList):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1
    msg = f"Set/Update field Upstream-Contact in debian/copyright."
    copyright_file = pkg.path/"debian/copyright"

    changed = False
    c = copyright.Copyright(copyright_file.open())
    if c.header.upstream_contact != tuple(contactList):
        c.header.upstream_contact = contactList
        copyright_file.write_text(c.dump())
        changed = True

    if changed:
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                           "debian/copyright"
                           ])
        pkg.git.index.commit(msg)


def cleanupMetadataObsoleteFields(pkg):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1

    metadata = pkg.path/"debian/upstream/metadata"

    with metadata.open() as f:
        data = yaml.safe_load(f)

    changed = False
    if 'Name' in data:
        del data['Name']
        changed = True
    if 'Contact' in data:
        del data['Contact']
        changed = True

    if changed:
       with metadata.open('w') as f:
           yaml.safe_dump(data, f)

       addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
       pkg.git.index.add(["debian/changelog",
               "debian/upstream/metadata",
               ])
       pkg.git.index.commit(msg)


def addMissingBugMetadatafields(pkg):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1

    metadata = pkg.path/"debian/upstream/metadata"

    api = projectsApi(pkg)

    if not api.get('bug_database'):
        return

    expected = {'Bug-Database': api['bug_database'],
                'Bug-Submit': api['bug_submit'],
               }

    with metadata.open() as f:
        data = yaml.safe_load(f)

    changed = False
    changed_add = False

    for field, value in expected.items():
        if data.get(field) != value:
            changed_add = not field in data
            data[field] = value
            changed = True

    if changed:
        with metadata.open('w') as f:
            yaml.safe_dump(data, f)

        if changed_add:
            msg = "Add Bug-* entries to metadata file."
        else:
            msg = "Update Bug-* entries to metadata file."
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                        "debian/upstream/metadata",
                        ])
        pkg.git.index.commit(msg)

def updateRepositoryMetadatafields(pkg):
    if not pkg.readyForChanges:
        print(f'Can\'t modify package("{pkg.name}"), cause stage is not clean or no open changelog entry.')
        return -1

    metadata = pkg.path/"debian/upstream/metadata"

    api = projectsApi(pkg)
    url = f"https://invent.kde.org/{api['repo']}"

    expected = {'Repository': f"{url}.git",
                'Repository-Browse': url,
                'Changelog': f"{url}/-/commits/master",
                }

    with metadata.open() as f:
        data = yaml.safe_load(f)

    changed = False
    changed_add = False

    for field, value in expected.items():
        if data.get(field) != value:
            data[field] = value
            changed = True

    if changed:
        with metadata.open('w') as f:
            yaml.safe_dump(data, f)

        msg = "Update repository related entries to metadata file."
        addChangeForMaintainer(pkg, f'  * {msg}', os.environ['DEBFULLNAME'])
        pkg.git.index.add(["debian/changelog",
                        "debian/upstream/metadata",
                        ])
        pkg.git.index.commit(msg)

if __name__ == "__main__":
    pkg = getPackage()
