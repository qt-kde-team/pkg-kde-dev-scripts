#!/bin/bash
set -e
set -o pipefail

# This script clones the Salsa repository and gets the upstream source from KDE’s invent
#
# The script supports 3 modes
# - no arg : try cloning a package with the name of the current directory
#
#   ex: $path/routine-clone
#
# - 1 arg: try cloning the package given in argument for the unstable distribution

#   ex: $path/routine-clone kcoreaddons
#
# - 2 args: try cloning the package given in 1st arg for the distribution given in 2nd arg
#           Only 2 distributions are supported : unstable (alias for 1 arg call) and experimental
#
#   ex: $path/routine-clone kcoreaddons experimental

. $(dirname $0)/routine-functions.sh

if [ $# -eq 0 ] ; then
  pkg_name=$(basename $PWD)
else
  pkg_name=$1
  [[ ! -d ${pkg_name} ]] && mkdir ${pkg_name}
  cd ${pkg_name}
fi

if [ $# -ge 2 ] ; then
  dist=$2
  if [ "${dist}" != "unstable" -a "${dist}" != "experimental" ] ; then
    >&2 echo "ERROR: 2nd argument for distribution must be either unstable or experimental"
    exit 1
  fi
else
  dist=unstable
fi

if [ "${dist}" == "experimental" ] ; then
  branch=debian/experimental
else
  branch=master
fi

if [ ! -d ${pkg_name}.salsa ] ; then
  echo "INFO: trying to clone Salsa repo to ${pkg_name}.salsa"
  if ! git clone "git@salsa.debian.org:qt-kde-team/kde/${pkg_name}.git" "${pkg_name}.salsa" ; then
    git clone "git@salsa.debian.org:qt-kde-team/extras/${pkg_name}.git" "${pkg_name}.salsa"
    git -C "${pkg_name}.salsa" checkout ${branch}
    git -C "${pkg_name}.salsa" pull
  fi
else
  salsa_orig=$(git -C "${pkg_name}.salsa" remote get-url origin)
  echo "INFO: git pulling from Salsa repo ${salsa_orig} into ${pkg_name}.salsa"
  git -C "${pkg_name}.salsa" checkout -B ${branch} origin/${branch}
  git -C "${pkg_name}.salsa" pull
fi

src_name=$(dpkg-parsechangelog -l ${pkg_name}.salsa/debian/changelog -S Source)
upstream_name=$(echo ${src_name} | sed -e "s/^kf6-//")
upstream_ver=$(dpkg-parsechangelog -l ${pkg_name}.salsa/debian/changelog -S Version | sed -e "s/^\(.*:\)*\([^-]\+\)-.*$/\2/")
if ! project_json=$(kde_project_api_get_json "${pkg_name}.salsa") ; then
  kde_project_api_get_json_error=${project_json}
  unset project_json
  >&2 echo "WARN: ${kde_project_api_get_json_error}"
fi
if [ ! -d "${upstream_name}.upstream" ] ; then
  if [ -z "${project_json}" ] ; then
    echo "WARN: ${kde_project_api_get_json_error}"
  else
    invent_git_repo="https://invent.kde.org/"$(echo ${project_json} | jq -r ".repo")".git"
    echo "INFO: git cloning upstream repo ${invent_git_repo} into ${upstream_name}.upstream"
    git clone "${invent_git_repo}" "${upstream_name}.upstream"
  fi
else
  invent_orig=$(git -C "${upstream_name}.upstream" remote get-url origin)
  echo "INFO: git pulling from ${invent_orig} to ${upstream_name}.upstream" 
  git -C "${upstream_name}.upstream" pull
fi

echo "INFO: getting source for package ${src_name}/${dist}, upstream version ${upstream_ver}"
[[ -n "${src_name}" ]] && [[ -n "${upstream_ver}" ]] && orig_dir="${src_name}-${upstream_ver}"
if [ ! -d "${orig_dir}" ] ; then
  apt source ${src_name}/${dist}
  [[ -d "${orig_dir}" ]] && rm -fr ${orig_dir}
  orig_pkg=$(find . -mindepth 1 -maxdepth 1 -name "${src_name}_${upstream_ver}.orig.tar*" -not -name "*.asc")
  if [ -z "${orig_pkg}" ] ; then
    >&2 echo "ERROR: couldn’t find upstream tarball matching ${src_name}_${upstream_ver}.orig.tar*"
    exit 1
  fi
  echo "INFO: extracting ${orig_pkg} to ${PWD}"
  tar xfa "${orig_pkg}"
fi

