#!/bin/bash
set -e
set -o pipefail

declare -A eventless_transitions=(
    [4.5.0]="4.5.1"
    [4.5.1]="4.6.0"
    [4.6.0]="4.6.1"
    [4.6.1]="4.6.2"
)

old_version=$(\grep -Eo "^Standards-Version:[[:space:]]+([[:digit:]\.]+)[[:space:]]*$" debian/control | sed -e "s/^Standards-Version:[[:space:]]\+\([[:digit:]\.]\+\)[[:space:]]*$/\1/")
new_version=${old_version}

test_next=1
while [[ "${test_next}" == 1 ]] ; do
  version_found=0
  for v in "${!eventless_transitions[@]}" ; do
    if [[ "${v}" == "${new_version}" ]] ; then
      next_new_version=${eventless_transitions[${v}]}
      version_found=1
      echo "INFO: Standards-Version ${new_version} can be bumped to ${next_new_version} automatically"
      new_version=${next_new_version}
      break
    fi
  done
  if [[ "${version_found}" != 1 ]] ; then
    test_next=0
  fi
done


if [[ "${new_version}" != "${old_version}" ]] ; then
  echo "INFO: Bumping Standards-Version from ${old_version} to ${new_version}"
  sed -i -e "s/^Standards-Version:[[:space:]]\+${old_version}/Standards-Version: ${new_version}/" debian/control
  msg="Bump Standards-Version to ${new_version}, no change required."
  dch -t --no-auto-nmu --multimaint-merge ${msg}
  git add debian/control debian/changelog
  git commit -m "${msg}"
  git --no-pager diff --word-diff HEAD~..HEAD
else
  result_std_ver=$(grep -E "^Standards-Version:" debian/control)
  echo "INFO: [debian/control] Leaving standards version as it is: ${result_std_ver}"
fi
