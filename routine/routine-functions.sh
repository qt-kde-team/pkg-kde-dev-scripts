#!/bin/bash

is_dep5_copyright () {
  local is_dep5
  grep -e "^Format:.*https\?://www.debian.org/doc/packaging-manuals/copyright-format/1.0/" debian/copyright > /dev/null 2>&1
  is_dep5=$(( $? ? 0 : 1 ))
  echo ${is_dep5}
}

# Try getting the project description from the KDE project API using in this order:
# - Upstream-Name field from d/copyright
# - Source package name from d/changelog
# - Last part of the Salsa repo origin URL
kde_project_api_get_json () {
  local deb_source_path=$1
  if [ -z "${deb_source_path}" ] ; then
    deb_source_path=${PWD}
  fi
  local upstream_name
  if upstream_name=$(\grep -e "^Upstream-Name:" ${deb_source_path}/debian/copyright) ; then
    upstream_name="${upstream_name##Upstream-Name: }"
    upstream_name="${upstream_name,,}"
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${upstream_name})
  fi
  if [ -z "${project_json}" ] ; then
    pkg=$(dpkg-parsechangelog -S Source -l "${deb_source_path}/debian/changelog")
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${pkg})
  fi
  if [ -z "${project_json}" ] ; then
    salsa_remote_basename=$(git -C "${deb_source_path}" remote get-url origin | sed -e "s=.*/\([^/]\+\).git=\1=")
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${salsa_remote_basename})
  fi
  if [ -n "${project_json}" ] ; then
    echo "${project_json}"
  else
    echo "could not find project in KDE API, neither as '${upstream_name}' from d/copyright, as '${pkg}' from d/changelog or as '${salsa_remote_basename}' from the Salsa repo name"
    return 1
  fi
}

kde_patch_version_from_debian_version() {
  local kde_version=$(echo $1 | sed -e 's/^\([[:digit:]]*:\)\?\(.*\)\([-~].*\)$/\2/')
  awk -F. '{print $NF}' <<< ${kde_version}
}

upstream_pkg_from_source_name() {
  local source_pkg_name=$1

  local base_repo_path=$(dirname $0)/..
  local upstream_data_files=$(compgen -G "${base_repo_path}"'/tierdata/*_upstream.yml')
  local upstream_pkg_name=$(sed -n "/^upstream_to_src_pkg:/,/^[^[:space:]]/s/^[[:space:]]\+\([^:]\+\):[[:space:]]*${source_pkg_name}[[:space:]]*$/\1/p" ${upstream_data_files})
  [[ -z "${upstream_pkg_name}" ]] && upstream_pkg_name=${source_pkg_name#kf6-}

  echo "${upstream_pkg_name}"
}

create_debian_orig_symlinks() {
  local source_pkg_name=$1
  local upstream_pkg_name=$2

  local upstream_tar_name=${upstream_pkg_name}-${upstream_version}.tar.xz
  local upstream_sig_name=${upstream_pkg_name}-${upstream_version}.tar.xz.sig

  local debian_orig_tar_name=${source_pkg_name}_${upstream_version}.orig.tar.xz
  local debian_orig_sig_name=${source_pkg_name}_${upstream_version}.orig.tar.xz.asc

  local error_upstream_tar_not_found=0
  if [[ -e "${upstream_tar_name}" ]] ; then
    echo "INFO: upstream tar found: ${PWD}/${upstream_tar_name}"
    if [[ ! -e "${debian_orig_tar_name}" ]] ; then
      ln -sfnv "${upstream_tar_name}" "${debian_orig_tar_name}"
    fi
    if [[ -e "${upstream_sig_name}" ]] ; then
      echo "INFO: upstream signature found: ${PWD}/${upstream_sig_name}"
      if [[ ! -e "${debian_orig_sig_name}" ]] ; then
        ln -sfnv "${upstream_sig_name}" "${debian_orig_sig_name}"
      fi
    fi
  else
    echo "ERROR: for source ${source_pkg_name}, upstream ${upstream_pkg_name} tar not found: ${PWD}/${upstream_tar_name}" >&2
    error_upstream_tar_not_found=1
  fi
  return ${error_upstream_tar_not_found}
}

