#!/bin/bash
set -e
set -o pipefail

TEMP_OPTS=$(getopt -o '' --long 'dry-run,experimental' -n "$0" -- "$@")
if [ $? -ne 0 ]; then
  echo 'Failed parsing arguments!' >&2
  exit 2
fi
eval set -- "$TEMP_OPTS"
unset TEMP_OPTS

dry_run=0
debian_suite="unstable"

while true; do
  case $1 in
    '--dry-run') dry_run=1 ; shift ;;
    '--experimental') debian_suite="experimental" ; shift ;;
    '--') shift ; break ;;
    *) echo 'Internal error parsing arguments!' >&2 ; exit 3 ;;
  esac
done

if [[ $# -ne 1 ]] ; then
  echo "Usage:"
  echo "    $0 [--experimental] version"
  echo "For example:"
  echo "    $0 5.101.0"
  echo "    $0 --experimental 5.104.0"
  exit 2
fi

tier_type=$(basename ${PWD})
if [[ "${tier_type}" != "frameworks" && "${tier_type}" != "plasma" && "${tier_type}" != "pim" && "${tier_type}" != "gear" ]] ; then
  echo "ERROR: current directory is not a supported tier type (frameworks, plasma, pim or gear)" >&2
  exit 1
fi

upstream_version=$1

################
# Load tier data
tier_file_name=tierdata/${tier_type}.${upstream_version}.tier.sh
# also try to find tier file without the latest bit (like 22.12 for 22.12.0)
tier_file_name_alt=tierdata/${tier_type}.${upstream_version%.*}.tier.sh
base_repo_path=$(dirname $0)/..
if [[ -e "${base_repo_path}/${tier_file_name}" ]] ; then
  . ${base_repo_path}/${tier_file_name}
elif [[ -e "${base_repo_path}/${tier_file_name_alt}" ]] ; then
  . ${base_repo_path}/${tier_file_name_alt}
else
  echo "ERROR: neither \"${tier_file_name}\" nor \"${tier_file_name_alt}\" tier files found, cannot create Debian orig symlinks." >&2
  exit 1
fi

declare -n tiers_array="${tier_type^^}TIERS"
for tier in $(seq 0 $(( "${#tiers_array[@]}" - 1 ))) ; do
  echo "INFO: dput-ing tier ${tier} packages…"
  for pkg in $(echo ${tiers_array[${tier}]}) ; do
    source_name=$(dpkg-parsechangelog -l ${pkg}/debian/changelog -S Source)
    source_version=$(dpkg-parsechangelog -l ${pkg}/debian/changelog -S Version)
    no_epoch_ver=${source_version#*:}
    source_changes=${tier_type}-builds/${source_name}_${no_epoch_ver}_source.changes
    if [[ ! -e "${source_changes}" ]] ; then
      echo "ERROR: source changes not found: ${source_changes}" >&2
      exit 3
    fi
    source_changes_pattern="^ ${source_name} \(([[:digit:]]+:)?${upstream_version}-[[:alnum:]\+\.~]+\) ${debian_suite}; urgency=medium$"
    if ! awk '/^Changes:$/,/^ /' ${source_changes} | grep -E "${source_changes_pattern}" ; then
      echo "ERROR: expected \"Changes:\" pattern not found in source changes file '${source_changes}':"
      echo "${source_changes_pattern}"
      exit 4
    fi
    dput_command="dput ${source_changes}"
    if [ ${dry_run} = 0 ] ; then
      ${dput_command}
    else
      echo "${dput_command}"
    fi
  done
done
