#!/bin/bash
# Script to update debian/*.install dh_install files from the installation
# errors found in the package build logs.
# Currently the script only supports removing entries from debian/*.install
# matching dh_install "missing files" warning.
# The script tries to autodetect the build log location, or alternatively a
# custome build log path can be given in argument
set -ex
set -o pipefail
shopt -s lastpipe # so that the changed=1 inside a pipe is not executed in a subshell and actually changes the value of the `changed' variable.

pkg=$(dpkg-parsechangelog -S Source)
ver=$(dpkg-parsechangelog -S Version)
dpkg_arch=$(dpkg --print-architecture)

if [ -n "$1" ] ; then
  if [ -e "$1" ] ; then
    build_log=$1
  else
    >&2 echo "ERROR: provided build log file name '$1' doesn’t exist"
    exit 1
  fi
else
  parent_dir=$(dirname $PWD)
  tiers_type=${parent_dir##*/} # frameworks, plasma or apps

  autodetected_build_logs=("../${pkg}_${ver}_${dpkg_arch}.build" # sbuild default
                           "../${tiers_type}-builds/${pkg}.build.log" # build-tiers success location
                           "../${tiers_type}-builds/${pkg}.FAILED.build.log") # build-tiers failure location
  for bl_idx in ${!autodetected_build_logs[*]} ; do
    cur_build_log="${autodetected_build_logs[bl_idx]}"
    if [ -e "${cur_build_log}" ] ; then
      build_log=${cur_build_log}
      echo "INFO: Using autodetected build log '${build_log}'"
      break
    fi
  done
fi

changed=0
\grep -E "^dh_install: warning: [^[:space:]]+ missing files: [^[:space:]]+$" ${build_log} \
  | while IFS= read -r build_install_log ; do
    package_with_missing=$(sed -e "s/^dh_install: warning: \([^[:space:]]\+\) missing files: [^[:space:]]\+$/\1/" <<< ${build_install_log})
    missing_file=$(sed -e "s/^dh_install: warning: [^[:space:]]\+ missing files: \([^[:space:]]\+\)$/\1/" <<< ${build_install_log})
    missing_file_grep_pattern=$(sed 's/*/\\*/g' <<< ${missing_file})
    missing_file_pattern=$(sed 's/\//\\\//g' <<< ${missing_file_grep_pattern})
    missing_file_pattern="^${missing_file_pattern}$"
    pkg_install_file="debian/${package_with_missing}.install"
    if grep -q -e "^${missing_file_grep_pattern}$" ${pkg_install_file} ; then
      echo "INFO: Removing installation of ${missing_file} from ${pkg_install_file}"
      sed -i "/${missing_file_pattern}/"'d' ${pkg_install_file}
      changed=1
    fi
  done

if [ "${changed}" -eq 1 ] ; then
  symbols_msg="Update the list of installed files from build logs."
  dch -t --no-force-save-on-release ${symbols_msg}
  git add debian/*.install debian/changelog
  git commit -m "${symbols_msg}"
  git --no-pager diff --word-diff HEAD~..HEAD
fi
