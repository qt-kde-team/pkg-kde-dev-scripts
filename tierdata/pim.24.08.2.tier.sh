PIMTIERS[0]="akonadi
    grantleetheme
    kldap
    kmime
    kontactinterface
    kpimtextedit
    kpkpass
    ksmtp
    libkdepim
    libkgapi"

PIMTIERS[1]="akonadi-contacts
    akonadi-mime
    akonadi-notes
    kidentitymanagement
    kimap
    kitinerary
    kmailtransport
    kmbox
    libkleo"

PIMTIERS[2]="akonadi-search
    kcalutils
    kmail-account-wizard
    mimetreeparser"

PIMTIERS[3]="kalarm
    kleopatra
    ktnef
    pimcommon"

PIMTIERS[4]="kaddressbook
    kontact
    libgravatar
    libksieve
    mailimporter"

PIMTIERS[5]="messagelib
    pim-sieve-editor"

PIMTIERS[6]="akonadi-calendar
    akregator
    grantlee-editor
    mailcommon"

PIMTIERS[7]="akonadi-import-wizard
    calendarsupport
    kdepim-runtime
    kmail
    mbox-importer
    merkuro
    pim-data-exporter
    zanshin"

PIMTIERS[8]="akonadi-calendar-tools
    akonadiconsole
    eventviews"

PIMTIERS[9]="incidenceeditor"

PIMTIERS[10]="kdepim-addons
    korganizer"

