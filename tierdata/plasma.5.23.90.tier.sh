PLASMATIERS[0]="bluedevil
    breeze-grub
    breeze-plymouth
    drkonqi
    kactivitymanagerd
    kdecoration
    kgamma5
    kinfocenter
    kmenuedit
    ksshaskpass
    kwallet-pam
    kwayland-integration
    kwayland-server
    kwrited
    layer-shell-qt
    libkscreen
    libksysguard
    milou
    plasma-discover
    plasma-disks
    plasma-firewall
    plasma-nano
    plasma-nm
    plasma-pa
    plasma-sdk
    plasma-thunderbolt
    plasma-workspace-wallpapers
    plymouth-kcm
    polkit-kde-agent-1
    qqc2-breeze-style
    sddm-kcm
    xdg-desktop-portal-kde
    "

# for now we drop ksysguard since it is not distributed with plasma
# but there will be a version that works with the new layout at some point
#    ksysguard
PLASMATIERS[1]="breeze
    kde-gtk-config
    kscreenlocker
    kscreen
    ksystemstats
    oxygen
    plasma-systemmonitor
    plasma-vault
    "

PLASMATIERS[2]="breeze-gtk
    kwin
    plasma-integration
    "

PLASMATIERS[3]="plasma-workspace
    "

PLASMATIERS[4]="kde-cli-tools
    kdeplasma-addons
    khotkeys
    plasma-browser-integration
    plasma-desktop
    powerdevil
    systemsettings
    "

