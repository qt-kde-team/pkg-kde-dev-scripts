PLASMATIERS[0]="bluedevil
    breeze-grub
    breeze-plymouth
    drkonqi
    kactivitymanagerd
    kdecoration
    kgamma5
    kinfocenter
    kmenuedit
    kscreenlocker
    ksshaskpass
    kwallet-pam
    kwayland-integration
    kwayland-server
    kwrited
    libkscreen
    libksysguard
    milou
    plasma-browser-integration
    plasma-discover
    plasma-disks
    plasma-nano
    plasma-nm
    plasma-pa
    plasma-sdk
    plasma-thunderbolt
    plasma-workspace-wallpapers
    plymouth-kcm
    polkit-kde-agent-1
    sddm-kcm
    xdg-desktop-portal-kde
    "

PLASMATIERS[1]="breeze
    kde-gtk-config
    kscreen
    ksysguard
    oxygen
    plasma-vault
    "

PLASMATIERS[2]="breeze-gtk
    kwin
    plasma-integration
    "

PLASMATIERS[3]="plasma-workspace
    "

PLASMATIERS[4]="kde-cli-tools
    kdeplasma-addons
    khotkeys
    plasma-desktop
    powerdevil
    systemsettings
    "

