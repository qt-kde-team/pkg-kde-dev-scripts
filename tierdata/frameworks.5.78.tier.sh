FRAMEWORKTIERS[0]="extra-cmake-modules
    kapidox
    "

FRAMEWORKTIERS[1]="attica
    bluez-qt
    breeze-icons
    karchive
    kcalcore
    kcodecs
    kconfig
    kcoreaddons
    kdbusaddons
    kdnssd
    kguiaddons
    kholidays
    ki18n
    kidletime
    kitemmodels
    kitemviews
    kplotting
    kwayland
    kwidgetsaddons
    kwindowsystem
    modemmanager-qt
    networkmanager-qt
    oxygen-icons5
    prison
    solid
    sonnet
    syntax-highlighting
    threadweaver
    "

FRAMEWORKTIERS[2]="kactivities
    kauth
    kcompletion
    kcontacts
    kcrash
    kdoctools
    kfilemetadata
    kimageformats
    kjobwidgets
    knotifications
    kpeople
    kpty
    kunitconversion
    syndication
    "

FRAMEWORKTIERS[3]="kactivities-stats
    kconfigwidgets
    kdesignerplugin
    kglobalaccel
    kjs
    kpackage
    kservice
    "

FRAMEWORKTIERS[4]="kded
    kdesu
    kemoticons
    kiconthemes
    kirigami
    kjsembed
    ktextwidgets
    kwallet
    "

FRAMEWORKTIERS[5]="kxmlgui
    qqc2-desktop-style
    "

FRAMEWORKTIERS[6]="kbookmarks
    "

FRAMEWORKTIERS[7]="kio
    "

FRAMEWORKTIERS[8]="baloo
    kdav
    kdeclarative
    kinit
    knewstuff
    knotifyconfig
    kparts
    kxmlrpcclient
    purpose
    "

FRAMEWORKTIERS[9]="frameworkintegration
    kcmutils
    kdelibs4support
    kdewebkit
    khtml
    kmediaplayer
    kquickcharts
    kross
    ktexteditor
    plasma-framework
    "

FRAMEWORKTIERS[10]="krunner
    "

