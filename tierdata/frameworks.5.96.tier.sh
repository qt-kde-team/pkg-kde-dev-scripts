FRAMEWORKSTIERS[0]="extra-cmake-modules
    kapidox"

FRAMEWORKSTIERS[1]="attica
    bluez-qt
    breeze-icons
    karchive
    kcalcore
    kcodecs
    kconfig
    kcoreaddons
    kdbusaddons
    kdnssd
    kguiaddons
    kholidays
    ki18n
    kidletime
    kitemmodels
    kitemviews
    kplotting
    kwayland
    kwidgetsaddons
    kwindowsystem
    modemmanager-qt
    networkmanager-qt
    oxygen-icons5
    prison
    solid
    sonnet
    syntax-highlighting
    threadweaver"

FRAMEWORKSTIERS[2]="kactivities
    kauth
    kcompletion
    kcontacts
    kcrash
    kdoctools
    kfilemetadata
    kimageformats
    kjobwidgets
    knotifications
    kpeople
    kpty
    kunitconversion
    syndication"

FRAMEWORKSTIERS[3]="kactivities-stats
    kconfigwidgets
    kdesignerplugin
    kglobalaccel
    kjs
    kpackage
    kservice"

FRAMEWORKSTIERS[4]="kded
    kdesu
    kemoticons
    kiconthemes
    kirigami
    kjsembed
    ktextwidgets
    kwallet"

FRAMEWORKSTIERS[5]="kxmlgui
    qqc2-desktop-style"

FRAMEWORKSTIERS[6]="kbookmarks"

FRAMEWORKSTIERS[7]="kio"

FRAMEWORKSTIERS[8]="baloo
    kdav
    kdeclarative
    kinit
    knewstuff
    knotifyconfig
    kparts
    kxmlrpcclient
    purpose"

FRAMEWORKSTIERS[9]="frameworkintegration
    kcmutils
    kdelibs4support
    kdewebkit
    khtml
    kmediaplayer
    kquickcharts
    kross
    ktexteditor
    plasma-framework"

FRAMEWORKSTIERS[10]="krunner"

