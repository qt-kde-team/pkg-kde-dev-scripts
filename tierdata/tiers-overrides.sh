declare -A SALSA_REPO_TO_SOURCE_PKG_NAME
SALSA_REPO_TO_SOURCE_PKG_NAME=(
  [elisa]=elisa-player
  [kirigami]=kirigami2
  [ktp-kded-module]=ktp-kded-integration-module
  [libkdcraw]=libkf5kdcraw
  [libkexiv2]=libkf5kexiv2
  [libkipi]=libkf5kipi
  [libkmahjongg]=libkf5kmahjongg
  [libksane]=libkf5sane
  [spectacle]=kde-spectacle
  [syntax-highlighting]=ksyntax-highlighting
)

