PIMTIERS[0]="akonadi
    libkf5grantleetheme
    kldap
    kmime
    kontactinterface
    kpimtextedit
    kpkpass
    ksmtp
    libkf5libkdepim
    libkgapi"

PIMTIERS[1]="akonadi-contacts
    akonadi-mime
    akonadi-notes
    kidentitymanagement
    kimap
    kitinerary
    kmbox
    libkf5libkleo"

PIMTIERS[2]="akonadi-search
    kcalutils
    kmailtransport"

PIMTIERS[3]="akonadi-calendar
    kalarm
    kleopatra
    ktnef
    libkf5pimcommon"

PIMTIERS[4]="libkf5calendarsupport
    kaddressbook
    kdepim-runtime
    kmail-account-wizard
    knotes
    kontact
    libkf5gravatar
    libkf5ksieve
    libkf5mailimporter
    zanshin"

PIMTIERS[5]="akonadi-calendar-tools
    libkf5eventviews
    kf5-messagelib
    pim-sieve-editor"

PIMTIERS[6]="akonadiconsole
    akregator
    grantlee-editor
    libkf5incidenceeditor
    libkf5mailcommon"

PIMTIERS[7]="akonadi-import-wizard
    kmail
    korganizer
    mbox-importer
    merkuro
    pim-data-exporter"

PIMTIERS[8]="kdepim-addons"

