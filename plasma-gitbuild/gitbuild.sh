#!/bin/bash

# bail on errors
set -e
# treat unset variables as errors
set -u
# fail if part of pipe fails
set -o pipefail

# You most probably want to update these paths to use the script ;-)
get_debian_dir() {
  echo "$HOME/Development/OSC/debian-kde/git-repos/plasma/$1.git"
}
get_upstream_dir() {
  echo "$HOME/Development/OSC/debian-kde/git-repos/upstream/plasma/$1.git"
}


pull=no
if [ "$1" = "-pull" ] ; then
  pull=yes
  shift
fi

CURR=$(pwd)

# packages that don't have locale files (.mo) so we need to update the .install files
remove_mo="breeze
    kde-cli-tools
    kdecoration
    kdeplasma-addons
    khotkeys
    kscreenlocker
    kwin
    libksysguard
    oxygen
    plasma-desktop
    plasma-discover
    plasma-nm
    plasma-workspace
    powerdevil"

# order of builds, taken from the plasma5.23.tiers.sh
build_order="bluedevil
    breeze-grub
    breeze-plymouth
    drkonqi
    kactivitymanagerd
    kdecoration
    kgamma5
    kmenuedit
    ksshaskpass
    kwallet-pam
    kwayland-integration
    kwayland-server
    kwrited
    layer-shell-qt
    libkscreen
    libksysguard
    milou
    plasma-discover
    plasma-disks
    plasma-firewall
    plasma-nano
    plasma-nm
    plasma-pa
    plasma-sdk
    plasma-thunderbolt
    plasma-workspace-wallpapers
    plymouth-kcm
    polkit-kde-agent-1
    qqc2-breeze-style
    sddm-kcm
    xdg-desktop-portal-kde
    breeze
    kde-gtk-config
    kscreenlocker
    kscreen
    ksystemstats
    oxygen
    plasma-systemmonitor
    plasma-vault
    breeze-gtk
    kinfocenter
    kwin
    plasma-integration
    plasma-workspace
    kde-cli-tools
    kdeplasma-addons
    khotkeys
    plasma-browser-integration
    plasma-desktop
    powerdevil
    systemsettings"


if [ "$1" = "-all" ] ; then
  PACKAGES_TODO="$build_order"
  shift
else
  PACKAGES_TODO="$1"
fi


do_one() {
  pkg="$1"
  
  if [ -z "$pkg" ] ; then
    echo "Need package as first argument" >&2
    exit 1
  fi
  
  debdir=$(get_debian_dir $pkg)
  updir=$(get_upstream_dir $pkg)
  
  if [ ! -d $debdir ] ; then
    echo "Cannot find debian package dir $debdir" >&2
    return 1
  fi
  if [ ! -d $updir ] ; then
    echo "Cannot find upstream package dir $updir" >&2
    return 1
  fi
  
  bl=$CURR/$pkg.build.log
  
  echo "BUILD LOG $pkg" > $bl
  
  echo "UPDATING debian directory ..."
  cd $debdir
  git checkout plasma523 &>> $bl
  [ $pull = yes ] && git pull &>> $bl
  
  cd $updir
  git checkout Plasma/5.23 &>> $bl
  git reset --hard &>> $bl
  git clean -xdf &>> $bl
  git clean -xdf &>> $bl
  [ $pull = yes ] && git pull &>> $bl
  cp -a $debdir/debian .
  
  for i in $remove_mo ; do
    if [ $i = $pkg ] ; then
      echo "REMOVING /usr/share/locale entries from install files ..."
      for j in debian/*.install ; do
        sed -i '/usr\/share\/locale/d' $j
      done
    fi
  done
  
  echo "BUILDING package ..."
  ymd=$(date +%Y%m%d)
  # some things that need to be done
  if [ $pkg = kwin ] ; then
    # drop upstream patch
    sed -i '/upstream-5b794ec1-fix-32bit-compile.patch/d' debian/patches/series
  fi
  if [ $pkg = libkscreen ] ; then
    cp $CURR/git-patches/libkscreen-symbols.patch .
    sed -i -e "s/YYYYMMDD/$ymd/g" libkscreen-symbols.patch
    patch -p0 < libkscreen-symbols.patch
    # rm libkscreen-symbols.patch
  fi
  if [ $pkg = "plasma-workspace" ] ; then
    # This is only due to missing mo files and should not be necessary
    # for released packages
    cp $CURR/git-patches/plasma-workspace_enable_debianabimanager.diff debian/patches/enable_debianabimanager.diff
  fi
  dch -v $(dpkg-parsechangelog -SVersion | sed -e "s/5\.22\.90-/5.22.90+${ymd}-/") "rebuild from git $ymd"
  v=$(dpkg-parsechangelog -SVersion)
  echo "* building $pkg $v, log in $bl"
  if [ -r debian/patches/series ] ; then
    if ! quilt push -a &>> $bl ; then
      echo "FAILED patching!" >> $bl
      echo "  failed!"
      return 1
    fi
  fi
  if dpkg-buildpackage -us -uc -rfakeroot -b &>> $bl ; then
    echo "  success!"
  else
    echo "  failed!"
  fi
  cd $CURR
}


for p in $PACKAGES_TODO ; do
  do_one $p
done
